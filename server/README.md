### GET  /WORKOUTS  --> Gets all workout documents
* http://localhost:8080/api/workouts

### POST  /WORKOUTS  --> Create new workout document
* http://localhost:8080/api/workouts

### GET  /WORKOUTS/:id  --> Get single workout document
* http://localhost:8080/api/workouts

### DELETE  /WORKOUTS/:id  -->  Delete a single workout
* http://localhost:8080/api/workouts/:id

### PATCH  /WORKOUTS/:id  --> Update a single workout
* http://localhost:8080/api/workouts/:id
