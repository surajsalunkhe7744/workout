import { useState } from "react";

type workout = {
  title: string;
  reps: number;
  load: number;
};

export const WorkoutForm = () => {
  const [workout, setWorkout] = useState<workout>({
    title: "",
    reps: 0,
    load: 0,
  });
  const [errorMessage, setErrorMessage] = useState<null | string>(null);

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    const response = await fetch("http://localhost:8080/api/workouts", {
      method: "POST",
      body: JSON.stringify(workout),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const json = await response.json();

    if (!response.ok) {
      setErrorMessage(json.error);
    } else {
      setErrorMessage(null);
      setWorkout({
        title: "",
        reps: 0,
        load: 0,
      });
    }
  };

  return (
    <form className="create" onSubmit={handleSubmit}>
      <h3>Add a New Workout</h3>

      <label>Exersize Title:</label>
      <input
        type="text"
        value={workout.title}
        onChange={(e) => setWorkout({ ...workout, title: e.target.value })}
      />

      <label>Load (in kg):</label>
      <input
        type="number"
        value={workout.load}
        onChange={(e) =>
          setWorkout({ ...workout, load: parseFloat(e.target.value) })
        }
      />

      <label>Reps:</label>
      <input
        type="number"
        value={workout.reps}
        onChange={(e) =>
          setWorkout({ ...workout, reps: parseFloat(e.target.value) })
        }
      />

      <button>Add Workout</button>
      {errorMessage && <div className="error">{errorMessage}</div>}
    </form>
  );
};
