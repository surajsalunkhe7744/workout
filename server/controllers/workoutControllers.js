const Workout = require("../models/Workout");

// Get all the workouts

const getWorkouts = async (req, res) => {
  try {
    const workouts = await Workout.find({}).sort({ createdAt: -1 });
    res.status(200).json(workouts);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Get a single workout

const getWorkout = async (req, res) => {
  try {
    const { id } = req.params;

    const workout = await Workout.findById(id);
    workout
      ? res.status(200).json(workout)
      : res.status(400).json({ error: "Sorry..., not found" });
  } catch (error) {
    res.status(400).json({ error: "No such workout" });
  }
};

// Create a workout

const createWorkout = async (req, res) => {
  const { body } = req;

  try {
    const workout = await Workout.create(body);
    res.status(200).json(workout);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Delete a workout

const deleteWorkout = async (req, res) => {
  const { id } = req.params;

  try {
    const workout = await Workout.findOneAndDelete({ _id: id });
    workout
      ? res.status(200).json(workout)
      : res.status(400).json({ error: "Sorry..., not found" });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Update the workout

const updateWorkout = async (req, res) => {
  const { id } = req.params;
  const { body } = req;

  try {
    const workout = await Workout.findOneAndUpdate({ _id: id }, { ...body });
    workout
      ? res.status(200).json(workout)
      : res.status(400).json({ error: "Sorry..., not found" });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

module.exports = {
  createWorkout,
  getWorkouts,
  getWorkout,
  deleteWorkout,
  updateWorkout
};
