import { formatDistanceToNow } from "date-fns";

type WorkoutDetailsData = {
  title: string;
  load: number;
  reps: number;
  createdAt: string;
  updatedAt: string;
  _id: string;
};

type WorkoutDetailsProps = {
    key: string,
    workout: WorkoutDetailsData
}

export const WorkoutDetails = ({key, workout}: WorkoutDetailsProps) => {
  const { title, load, reps, createdAt, _id } = workout;

  const handleDelete = async () => {
    const response = await fetch(`http://localhost:8080/api/workouts/${_id}`,{
        method: "DELETE",
    })

    // const json = await response.json();

    if(response.ok) {
        console.log("Workout Deleted");
    }
  }

  return (
    <div className="workout-details">
      <h4>{title}</h4>
      <p>
        <strong>Load (kg): </strong>
        {load}
      </p>
      <p>
        <strong>Reps: </strong>
        {reps}
      </p>
      <p>{formatDistanceToNow(new Date(createdAt),{addSuffix: true})}</p>
      <span className="material-symbols-outlined" onClick={handleDelete}>delete</span>
    </div>
  );
};
