import { useEffect, useState } from "react";
import { WorkoutDetails } from "../components/WorkoutDetails";
import { WorkoutForm } from "../components/WorkoutForm";

type workoutFields = {
  title: string;
  load: number;
  reps: number;
  _id: string;
  createdAt: string;
  updatedAt: string;
};

export const Home = () => {
  const [workouts, setWorkouts] = useState<null | workoutFields[]>(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    fetch("http://localhost:8080/api/workouts")
      .then((workoutData) => workoutData.json())
      .then((workoutData) => {
        setWorkouts(workoutData);
        setIsLoading(false);
      })
      .catch((error) => console.log(error.message));
  }, []);

  return (
    <div className="home">
      {isLoading && <h2>Loading ...</h2>}

      {workouts?.length === 0 && <h2>No workouts found ...</h2>}

      <div className="workouts">
        {workouts &&
          workouts.map((workout) => {
            const {_id} = workout;
            return <WorkoutDetails key={_id} workout={workout}/>;
          })}
      </div>
      
      {!isLoading && <WorkoutForm />}
    </div>
  );
};
